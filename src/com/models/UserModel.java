package com.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

public class UserModel {

	
	private String name;
	private String email;
	private String pass;
	private Integer type ;
	private Integer id;
	private Double lat;
	private Double lon;
	
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getType() {
		return type;
	}
	public String getPass(){
		return pass;
	}
	
	public void setPass(String pass){
		this.pass = pass;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public static ResponseModel addStore (Integer userId,String name ,String address ,int type ,String desc)
	{
		ResponseModel response =new ResponseModel();
		response.setValue(false);
		response.setMsg("not sucessful");
		
		try {
			Connection conn = DBConnection.getActiveConnection();
			String sql = "Insert into stores (`user_id` ,`name` ,`address` ,`type` ,`desc` ) VALUES  (?,?,?,?,?)";
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, userId);
			stmt.setString(2, name);
			stmt.setString(3, address);
			stmt.setInt(4, type);
			stmt.setString(5, desc);
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				response.setValue(true);
				response.setMsg("you added store sucessfuly");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}
	public static ResponseModel getStat(int storeId)
	{
		return new ResponseModel();
	}
	public static ResponseModel addBrand (String name)
	{
		ResponseModel response =new ResponseModel();
		response.setValue(false);
		if(name.equals(""))
		{
			response.setMsg("invalid name");
			response.setValue(true);
			return response;
		}
		else
		{
			try {
				Connection conn = DBConnection.getActiveConnection();
				String sql = "Insert into brand (`name`) VALUES  (?)";
				// System.out.println(sql);

				PreparedStatement stmt;
				stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				stmt.setString(1, name);
				stmt.executeUpdate();
				ResultSet rs = stmt.getGeneratedKeys();
				if (rs.next()) {
					response.setMsg("you add brand successfuly");
					response.setValue(true);
					return response;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		response.setMsg("the brand is already exist");
		response.setValue(true);
		return response ;
	}
	public static ResponseModel addProduct (String name ,int price)
	{
		ResponseModel response =new ResponseModel();
		response.setValue(false);
		if(name.equals(""))
		{
			response.setMsg("invalid name");
			response.setValue(true);
			return response;
		}
		else
		{
			try {
				Connection conn = DBConnection.getActiveConnection();
				String sql = "Insert into product (`name` ,`price`) VALUES  (? ,?)";
				// System.out.println(sql);
				PreparedStatement stmt;
				stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				stmt.setString(1, name);
				stmt.setInt(2, price);
				stmt.executeUpdate();
				ResultSet rs = stmt.getGeneratedKeys();
				if (rs.next()) {
					response.setMsg("you add product successfuly");
					response.setValue(true);
					return response;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return response ;
	}
	public static UserModel addNewUser(String name, String email, String pass ,int type) {
		try {
			Connection conn = DBConnection.getActiveConnection();
			String sql = "Insert into users (`name`,`email`,`pass` ,`type`) VALUES  (?,?,?,?)";
			// System.out.println(sql);

			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, name);
			stmt.setString(2, email);
			stmt.setString(3, pass);
			stmt.setInt(4, type);
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				UserModel user = new UserModel();
				user.id = rs.getInt(1);
				user.email = email;
				user.pass = pass;
				user.name = name;
				user.type= type;
				user.lat = 0.0;
				user.lon = 0.0;
				return user;
			}
			return null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	
	
	public static UserModel login(String email, String pass) {
		try {
			Connection conn = DBConnection.getActiveConnection();
			String sql = "Select * from users where `email` = ? and `pass` = ?";
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, email);
			stmt.setString(2, pass);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				UserModel user = new UserModel();
				user.id = rs.getInt(1);
				user.email = rs.getString("email");
				user.pass = rs.getString("pass");
				user.name = rs.getString("name");
				user.type=rs.getInt("type");;
				return user;
			}
			return null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static boolean updateUserPosition(Integer id, Double lat, Double lon) {
		try{
			Connection conn = DBConnection.getActiveConnection();
			String sql = "Update users set `lat` = ? , `long` = ? where `id` = ?";
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			stmt.setDouble(1, lat);
			stmt.setDouble(2, lon);
			stmt.setInt(3, id);
			stmt.executeUpdate();
			return true;
		}catch(SQLException e){
			e.printStackTrace();
		}
		return false;
	}

}
