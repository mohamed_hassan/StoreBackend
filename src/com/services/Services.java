package com.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.mvc.Viewable;
import org.json.simple.JSONObject;

import com.models.DBConnection;
import com.models.ResponseModel;
import com.models.UserModel;

@Path("/")
public class Services {

	/*
	 * @GET
	 * 
	 * @Path("/signup")
	 * 
	 * @Produces(MediaType.TEXT_HTML) public Response signUp(){ return
	 * Response.ok(new Viewable("/Signup.jsp")).build(); }
	 */

	@POST
	@Path("/signup")
	@Produces(MediaType.TEXT_PLAIN)
	public String signUp(@FormParam("name") String name,
			@FormParam("email") String email, @FormParam("pass") String pass , @FormParam("type") Integer type) {
		UserModel user = UserModel.addNewUser(name, email, pass ,type);
		JSONObject json = new JSONObject();
		if(user!=null)
		{
		json.put("id", user.getId());
		json.put("name", user.getName());
		json.put("email", user.getEmail());
		json.put("pass", user.getPass());
		json.put("type", user.getType());
		json.put("value",true);
		return json.toJSONString();
		}
		else
		{
			json.put("value",false);
			json.put("msg","there is some thing wrong please sure that you didn't register before and you insert valid mail");
			return json.toJSONString();
		}
	}

	@POST
	@Path("/login")
	@Produces(MediaType.TEXT_PLAIN)
	public String login(@FormParam("email") String email,
			@FormParam("pass") String pass) {
		UserModel user = UserModel.login(email, pass);
		JSONObject json = new JSONObject();
		if(user!=null)
		{
		json.put("id", user.getId());
		json.put("name", user.getName());
		json.put("email", user.getEmail());
		json.put("pass", user.getPass());
		json.put("value", true);
		}
		else
		{
			json.put("value", false);
			json.put("msg", "wrong email or password");
		}
		
		return json.toJSONString();
	}
	@POST
	@Path("/brand")
	@Produces(MediaType.TEXT_PLAIN)
	public String brand(@FormParam("name") String name) {	
	
		ResponseModel responseModel =UserModel.addBrand(name);
		JSONObject json = new JSONObject();
		json.put("msg", responseModel.getMsg());
		json.put("value", responseModel.isValue());
		return json.toJSONString();
	}
	@POST
	@Path("/product")
	@Produces(MediaType.TEXT_PLAIN)
	public String product(@FormParam("name") String name,@FormParam("price") Integer price) {	
	
		ResponseModel responseModel =UserModel.addProduct(name ,price);
		JSONObject json = new JSONObject();
		json.put("msg", responseModel.getMsg());
		json.put("value", responseModel.isValue());
		return json.toJSONString();
	}
	@POST
	@Path("/addstore")
	@Produces(MediaType.TEXT_PLAIN)
	public String addNewStore( @FormParam("userId") Integer userId,@FormParam("name") String name,@FormParam("address") String address 
			,@FormParam("type")Integer type ,@FormParam("desc")String desc) {	
	
		ResponseModel responseModel= UserModel.addStore( userId,name ,address ,type ,desc);
		JSONObject json = new JSONObject();
		json.put("msg", responseModel.getMsg());
		json.put("value", responseModel.isValue());
		return json.toJSONString();
	}

	
	@POST
	@Path("/statistics")
	@Produces(MediaType.TEXT_PLAIN)
	public String statistics(@FormParam("name") Integer storeId) {	
	
		ResponseModel responseModel =UserModel.getStat(storeId);
		JSONObject json = new JSONObject();
		json.put("msg", responseModel.getMsg());
		json.put("value", responseModel.isValue());
		return json.toJSONString();
	}


	@GET
	@Path("/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getJson() {
		return "Hello after editing";
		// Connection URL:
		// mysql://$OPENSHIFT_MYSQL_DB_HOST:$OPENSHIFT_MYSQL_DB_PORT/
	}
	@GET
	@Path("/test")
	@Produces(MediaType.TEXT_PLAIN)
	public String testServer() {
		return "test server";
		// Connection URL:
		// mysql://$OPENSHIFT_MYSQL_DB_HOST:$OPENSHIFT_MYSQL_DB_PORT/
	}
}
